<?php

use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('Utama', [BlogController::class, 'index'])->name('blog.index');
Route::get('Tampil/{id}', [BlogController::class, 'show']);
Route::get('TambahData', [BlogController::class, 'create'])->name('adddata.create');
Route::post('simpan-data', [BlogController::class, 'store'])->name('blog.store');
Route::get('ubahData/{id}', [BlogController::class, 'edit']);
Route::put('ubahData/{id}', [BlogController::class, 'update']);
Route::delete('delete/{id}', [BlogController::class, 'destroy']);

