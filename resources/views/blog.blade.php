<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Raport</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" >

<body>
    <table class="table table-striped" border="30">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Mapel</th>
            <th scope="col">Pengetahuan</th>
            <th scope="col">Keterampilan</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($blg as $item)
            <tr>
            <th scope="row">{{ $loop->iteration }}</th>
                <td>{{ $item->Mapel }}</td>
                <td>{{ $item->Pengetahuan }}</td>
                <td>{{ $item->Keterampilan }}</td>
                <td>
                    <form action="">
                        <button>
                            <a href="{{ url('ubahData', $item->id) }}">UBAH</a>
                        </button>
                    </form>
                </td>
                <td>
                    <form method="POST" action="{{ url('delete', $item->id) }}" id="hapus">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Hapus</button>
                    </form>
                </td>
                <td>
                    <button>
                        <a href="{{ url('Tampil', $item->id) }}">Tampilkan</a>
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
      <button style="margin-left: 650px"><a href="{{ route('adddata.create') }}">Tambah Data</a></button>
</body>

<script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
</html>
