<?php

namespace Database\Seeders;

use App\Models\Blog;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Blog();
        $data->create([
            'mapel' => 'Ekonomi',
            'Pengetahuan' => '81',
            'Keterampilan' => '80'
        ]);

        $data->create([
            'mapel' => 'Geografi',
            'Pengetahuan' => '83',
            'Keterampilan' => '86'
        ]);

        $data->create([
            'mapel' => 'PAI',
            'Pengetahuan' => '89',
            'Keterampilan' => '89'
        ]);
    }
}
