<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blg = Blog::all();

        return view('blog', compact('blg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adddata');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Blog();

        $save = $data->create([
            'Mapel' => $request->mapel,
            'Pengetahuan' => $request->pengetahuan,
            'Keterampilan' => $request->keterampilan
        ]);
        if($save) {
           $blg = Blog::all();
           return view('blog', compact('blg'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Blog::find($id);

        return view('tampilkandata', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Blog::find($id);

        return view('ubahdata', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {
        $data = Blog::find($id);
        // dd($data);
        $update = $data->update([
            'Mapel' => $request->mapel,
            'Pengetahuan' => $request->pengetahuan,
            'Keterampilan' => $request->keterampilan
        ]);

        if($update){
            $blg = Blog::all();
            return view('blog' , compact('blg'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Blog::find($id);

        $data->delete();

        return redirect()->route('blog.index');
    }
}
