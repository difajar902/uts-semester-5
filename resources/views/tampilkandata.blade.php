<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tampil Data</title>
</head>
<body>
    <form method="POST" action="">
        @csrf
        <table border="2">
            <tr>
                <td>Mapel</td>
                <td>Pengetahuan</td>
                <td>Keterampilan</td>
            </tr>
            <tr>
                <td>{{ $data->Mapel }}</td>
                <td>{{ $data->Pengetahuan }}</td>
                <td>{{ $data->Keterampilan }}</td>
            </tr>
        </table>
    </form>
</body>
</html>
