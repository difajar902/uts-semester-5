<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ubah Data</title>
</head>
<body>
    <form method="POST" action="{{ url('ubahData', $data->id) }}">
        @csrf
        @method('PUT')
        <table border="2">
            <tr>
                <td>Mapel</td>
                <td>Pengetahuan</td>
                <td>Keterampilan</td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="mapel" value="{{ $data->Mapel }}">
                </td>
                <td>
                    <input type="text" name="pengetahuan" value="{{ $data->Pengetahuan }}">
                </td>
                <td>
                    <input type="text" name="keterampilan" value="{{ $data->Keterampilan }}">
                </td>
                <td>
                    <button type="submit">UBAH</button>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
