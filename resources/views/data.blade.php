<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tampil Data</title>
</head>
<body>
    <table border="2">
        <tr>
            <td>Mapel</td>
            <td>Pengetahuan</td>
            <td>Keterampilan</td>
        </tr>
        <tr>
            @foreach ($data as $item)
                <td>{{ $data->Mapel }}</td>
                <td>{{ $data->Pengetahuan }}</td>
                <td>{{ $data->Keterampilan }}</td>
            @endforeach
        </tr>
    </table>
</body>
</html>
