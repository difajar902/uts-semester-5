<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tambah Data</title>
</head>
<body>
    <form method="POST" action="{{ route('blog.store') }}">
        @csrf
        <table border="2">
            <tr>
                <td>Mapel</td>
                <td>Pengetahuan</td>
                <td>Keterampilan</td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="mapel">
                </td>
                <td>
                    <input type="text" name="pengetahuan">
                </td>
                <td>
                    <input type="text" name="keterampilan">
                </td>
                <td>
                    <button type="submit">Save</button>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
